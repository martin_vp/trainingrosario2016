﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practico1
{
    public class Ejercicio1
    {
        public static int CalcularMenor(int primeroValor, int segundoValor, int tercerValor)
        {
            if (primeroValor < segundoValor)
            {
                if (primeroValor < tercerValor)
                {
                    return primeroValor;
                }
                else
                {
                    return tercerValor;
                }
            }
            else if (segundoValor < tercerValor)
            {
                return segundoValor;
            }
            else
            {
                return tercerValor;
            }
        }
    }
}
