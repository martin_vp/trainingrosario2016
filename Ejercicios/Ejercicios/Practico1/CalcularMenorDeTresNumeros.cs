﻿using System;
using Practico1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ejercicios.Practico1
{
    [TestClass]
    public class CalcularMenorDeTresNumeros
    {
        [TestMethod]
        public void UnoDosTresEsUno()
        {            
            Assert.AreEqual(1, Ejercicio1.CalcularMenor(1, 2, 3));
        }
       
        [TestMethod]
        public void CincoTresOchoEsTres()
        {
            Assert.AreEqual(3, Ejercicio1.CalcularMenor(5, 3, 8));
        }

        [TestMethod]
        public void NueveSeisUnoEsUno()
        {
            Assert.AreEqual(1, Ejercicio1.CalcularMenor(9, 6, 1));
        }

        [TestMethod]
        public void Menos5Menos3Menos1EsMenos5()
        {
            Assert.AreEqual(-5, Ejercicio1.CalcularMenor(-5, -3, -1));
        }        
    }
}
